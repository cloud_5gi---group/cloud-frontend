FROM ubuntu:16.04

RUN apt-get update && apt-get install -y software-properties-common && add-apt-repository -y ppa:alex-p/tesseract-ocr
RUN apt-get update && apt-get install -y tesseract-ocr-all
RUN apt-get -y install build-essential libssl-dev
RUN apt-get -y install curl
RUN apt-get -y autoclean


#copy traindata
COPY fra.traineddata /usr/share/tesseract-ocr/tessdata
RUN tesseract --version

#install node8
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN apt-get install -y nodejs

RUN node -v
RUN npm -v

RUN mkdir /home/work
WORKDIR /home/work

#copy the package json of the project
COPY package.json ./

#run npm install to install all dependency
RUN npm install

#copy all the file of the project
COPY . .

ENV BLOCKCHAIN_URL="http://localhost:3001"

EXPOSE 3000
CMD [ "npm", "start" ]