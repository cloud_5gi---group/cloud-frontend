require('dotenv/config');

module.exports = {
	blockchain_url: process.env.BLOCKCHAIN_URL || 'http://localhost:3001'
};