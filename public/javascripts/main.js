$(function () {
  "use strict";

  var randomScalingFactor = function () {
    return (
      (Math.random() > 0.5 ? 1.0 : 2.0) * Math.round(Math.random() * 100)
    );
  };

  var chartColors = {
    red: "rgb(255, 99, 132)",
    orange: "rgb(255, 159, 64)",
    yellow: "rgb(255, 205, 86)",
    green: "rgb(75, 192, 192)",
    blue: "rgb(54, 162, 235)",
    purple: "rgb(153, 102, 255)",
    grey: "rgb(231,233,237)"
  };

  var CANDIDATS = myObj.map(candidat => {
    return candidat.candidat;
  })
  console.log(CANDIDATS);

  var color = Chart.helpers.color;
  var barChartData = {
    labels: CANDIDATS,
    datasets: [
      {
        label: "Evaluation Election",
        backgroundColor: color(chartColors.red)
          .alpha(0.5)
          .rgbString(),
        borderColor: chartColors.red,
        borderWidth: 1,
        data: [
          randomScalingFactor(),
          randomScalingFactor(),
          randomScalingFactor(),
          randomScalingFactor(),
        ]
      }
    ]
  };
  myObj.forEach(parti => {  
    var ctx = document.getElementById(parti.nom + 'niv1').getContext("2d");
    window.myBar = new Chart(ctx, {
      type: "bar",
      data: barChartData,
      options: {
        responsive: true,
        legend: {
          position: "top"
        },
        title: {
          display: true,
          text: "Resultat Election"
        }
      }
    });
  })
});