const express = require("express");
const multer = require("multer");
const fs = require("fs");
const { exec } = require("child_process");
const rp = require("request-promise");
const { blockchain_url } = require("../config/");
const mkdirp = require("mkdirp");
require("dotenv/config");

let router, storage, upload;
storage = multer.diskStorage({
  destination: function(req, file, cb) {
    cb(null, "public/images/election");
  },
  filename: function(req, file, cb) {
    if (file.hasOwnProperty("originalname")) cb(null, file.originalname);
  }
});
upload = multer({ storage: storage });
router = express.Router();

/* Cette route c'est pour expliquer comment générer les pvs
 * */
router.get("/", function(req, res) {
  res.render("step1");
});

//Cette route c'est pour charger le fichier json qui contient la description de l'election
router.get("/step2", (req, res) => {
  res.render("step2");
});

//Cette route c'est pour récupérer le fichier json et pour chaque pvs recupérer les informations json et envoyer à la blockchain
router.post("/step2", upload.single("customfile"), async (req, res) => {
  if (!req.file) {
    console.log("here");
    return res.render("step2", { error: true });
  }
  const file = req.file;
  const path = file.path;
  console.log("path");
  let data = fs.readFileSync(path);
  data = JSON.parse(data);
  // pour chaque element du json envoyé les pvs à la blockchain
  if (data.hasOwnProperty("list_pvs")) {
    await Promise.all(
      data.list_pvs.map(async bv => {
        let pvs;
        if (bv.hasOwnProperty("pvs")) pvs = bv.pvs;

        await Promise.all(
          pvs.map(async pv => {
            let json_ocr;
            const { stdout, stderr } = await exec(
              "java -jar CloudTPParseImage.jar " + "../public/images/election/" + pv
            );
            if (!stderr) json_ocr = JSON.parse(stdout); // je récupère les données de l'ocr et je parse en json
            const requestOptions = {
              uri: blockchain_url + "/transaction/broadcast",
              method: "POST",
              body: {
                json_ocr: json_ocr
              },
              json: true
            };
            await rp(requestOptions);
          })
        );
        const options = {
          uri: blockchain_url + "/mine",
          headers: {
            "User-Agent": "Request-Promise"
          },
          json: true // Automatically parses the JSON string in the response
        };
        const temp = await rp(options);
      })
    );
  }
  return res.redirect("/step3");
});

//cette route c'est pour voir les differents candidats,
router.get("/step3", (req, res) => {
  let data = fs.readFileSync("public/images/election/json.json");
  let candidats, bureauVote, nombreInscrits, nombreParticipants;
  data = JSON.parse(data);
  if (data.hasOwnProperty("candidats")) {
    candidats = data.candidats;
  }
  if (data.hasOwnProperty("pvs")) {
    bureauVote = data.pvs;
  }
  if (data.hasOwnProperty("nombre_inscrits")) {
    nombreInscrits = data.nombre_inscrits;
  }
  if (data.hasOwnProperty("nombre_participants")) {
    nombreParticipants = data.nombre_participants;
  }
  console.log(candidats);
  res.render("step3", {
    candidats: candidats,
    step3: true,
    bureauVote: bureauVote,
    nombreInscrits: nombreInscrits,
    nombreParticipants: nombreParticipants
  });
});

//cette route c'est pour voir les resultats
router.get("/step4", async (req, res) => {
  let data = fs.readFileSync("public/images/election/json.json");
  let blockchain, options, listBlock, listPartis, listPvPartis = [], listCandidats, ResultatNiv1, ResultatNiv2, ResultatElecam;  
  options = {
    uri: blockchain_url + "/blockchain",
    headers: {
      "User-Agent": "Request-Promise"
    },
    json: true
  };
  blockchain = await rp(options);  
  if (blockchain.hasOwnProperty("chain")) listBlock = blockchain.chain.slice(1);
  console.log(listBlock);

  //récupère la liste des partis 
  data = JSON.parse(data);
  listCandidats = data.candidats;  
  listPartis = listCandidats.map(candidat => {
    const parti = candidat.nom;	      
    listPvPartis.push({[parti]: []})
	  return parti;
  });
//  console(listPvPartis);  

  // récupère les pvs de chaque partis
  /* listBlock.forEach(PvsBureauVote => {
	  listPartis.forEach(parti => {
      PvsBureauVote.forEach(pv => {
        if(pv.owner === parti){
          listPvPartis[parti].push(pv);
        }
      })
    })
  }); */
  console.log(listCandidats)
  console.log(listPartis)

  res.render("step4", {listCandidats: listCandidats, listPartis: listPartis, step4: true});
});

module.exports = router;
